import requests
import sys

file_name = sys.argv[1] 
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF'
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'


# Define the headers with the API token
headers = {
    'Authorization': 'Token f7ba178c88b45946c6cccd90267325563b88e1c3'
}

# Define the URL for importing scan results
url = 'https://demo.defectdojo.org/api/v2/import-scan/'

# Define the data to be sent in the request
data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'Low',
    'engagement': 14
}

# Define the file to be uploaded
files = {
    'file': open(file_name, 'rb')
}

# Make a POST request to import the scan results
response = requests.post(url, headers=headers, data=data, files=files)

# Check if the request was successful (status code 201)
if response.status_code == 201:
    print('Scan results imported successfully')
else:
    # If the request failed, print the error message
    print(f'Failed to import scan results: {response.text}')